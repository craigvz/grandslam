//
//  MyScene.m
//  GrandSlam
//
//  Created by Craig VanderZwaag on 8/18/13.
//  Copyright (c) 2013 blueHula Studios. All rights reserved.
//

#import "MyScene.h"

@interface MyScene () {
    
    CFTimeInterval startTime;
    CFTimeInterval touchTime;
    int rndValueStrike;
    int rndValueBallLeft;
    int rndValueBallRight;
    int rndValueBallHigh;
    int rndValueBallLow;
    
}

@property BOOL contentCreated;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UIButton *swingButton;


@end

@implementation MyScene

@synthesize timeLabel;
@synthesize swingButton;


- (void)didMoveToView: (SKView *) view
{
    if (!self.contentCreated)
    {
        [self createSceneContents];
        self.contentCreated = YES;
    }
}


- (void)createSceneContents
{
    self.backgroundColor = [SKColor colorWithRed:.196 green:.521 blue:.603 alpha:1.0];
    self.scaleMode = SKSceneScaleModeAspectFit;
    
    
    //Pitch Button
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(didTouchPitchButton)
     forControlEvents:UIControlEventTouchDown];
    [button setTitle:@"PITCH" forState:UIControlStateNormal];
    button.frame = CGRectMake(500, 650, 160.0, 60);
    [button.layer setBorderWidth:2];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:button];
    
    //Swing Button
    swingButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [swingButton addTarget:self
                    action:@selector(didTouchSwingButton)
          forControlEvents:UIControlEventTouchDown];
    [swingButton setTitle:@"SWING" forState:UIControlStateNormal];
    swingButton.frame = CGRectMake(500, 800, 160.0, 60);
    [swingButton.layer setBorderWidth:2];
    [swingButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:swingButton];
    
    //Reaction Time Label
    timeLabel = [ [UILabel alloc] initWithFrame:CGRectMake(400, 400, 400, 43.0) ];
    timeLabel.textColor = [UIColor whiteColor];
    timeLabel.font = [UIFont fontWithName:@"Helvetica" size:(24.0)];
    NSString *str = [NSString stringWithFormat:@"Reaction Time %.3fms", touchTime];
    timeLabel.text = [NSString stringWithFormat: @"%@", str];
    [self.view addSubview:timeLabel];
    
}

#pragma mark Nodes

- (SKLabelNode *)newHelloNode
{
    SKLabelNode *helloNode = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    helloNode.text = @"Grand Slam!";
    helloNode.fontSize = 42;
    helloNode.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame));
    helloNode.name = @"helloNode";
    return helloNode;
}

- (SKLabelNode *)newBaseHitLabelNode
{
    SKLabelNode *baseHitLabelNode = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    
    if (touchTime <= 0.2) {
        baseHitLabelNode.text = @"Home Run!";
    } else
        if ((touchTime > 0.2) && (touchTime <= 0.3)) {
            baseHitLabelNode.text = @"Triple!";
        }
        else
            if ((touchTime > 0.3) && (touchTime <= 0.4)) {
                baseHitLabelNode.text = @"Double";
            }
            else
                if ((touchTime > 0.4) && (touchTime <= 0.5)) {
                    baseHitLabelNode.text = @"Single!";
                }
                else
                    if ((touchTime > 0.5) && (touchTime <= 0.6)) {
                        baseHitLabelNode.text = @"Foul!";
                    }
                    else
                        if ((touchTime > 0.4) && (touchTime <= 0.5)) {
                            baseHitLabelNode.text = @"Grand Slam!";
                        } else
                            baseHitLabelNode.text = @"Strike!";
    
    
    
    baseHitLabelNode.fontSize = 42;
    baseHitLabelNode.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame));
    baseHitLabelNode.name = @"baseHit";
    
    return baseHitLabelNode;
}

- (SKLabelNode *)newSwingAndMissLabelNode
{
    SKLabelNode *swingAndMissLabelNode = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    swingAndMissLabelNode.fontSize = 42;
    swingAndMissLabelNode.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame));
    swingAndMissLabelNode.text = @"STRIKE!";
    swingAndMissLabelNode.name = @"swingMiss";
    
    return swingAndMissLabelNode;
    
}

- (SKLabelNode *)newBallLabelNode
{
    SKLabelNode *newBallLabelNode = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    newBallLabelNode.fontSize = 42;
    newBallLabelNode.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame));
    newBallLabelNode.text = @"BALL!";
    newBallLabelNode.name = @"ball";
    
    return newBallLabelNode;
    
}


- (SKShapeNode *)newShapeNodeLeft {
    
    int radius = 25;
    
    //Generate random number for start time
    int lowerBound = 1000;
    int upperBound = 5000;
    int rndLeftBallFlashCount = lowerBound + arc4random() % (upperBound - lowerBound);
    
    //Create Node
    SKShapeNode *strikeNode = [[SKShapeNode alloc]init];
    strikeNode.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 2.0*radius, 2.0*radius)
                                                 cornerRadius:radius].CGPath;
    
    strikeNode.position = CGPointMake(150, 250);
    strikeNode.name = @"ballLeft";
    
    //Set color
    if (rndLeftBallFlashCount & 1) {
        strikeNode.fillColor = [UIColor greenColor];
    } else {
        strikeNode.fillColor = [UIColor redColor];
    }
    
    //Handle Node animation
    SKAction *blink = [SKAction sequence:@[
                                           [SKAction fadeOutWithDuration:0.05],
                                           [SKAction waitForDuration:0.5],
                                           [SKAction fadeInWithDuration:0.05],
                                           [SKAction waitForDuration:0.5]]];
    
    SKAction *fade = [SKAction sequence:@[[SKAction waitForDuration:5.0],
                                          [SKAction fadeOutWithDuration:0.05]]];
    
    SKAction *blinkRandomly = [SKAction repeatAction:blink count:(rndLeftBallFlashCount/1000)];
    [strikeNode runAction:fade];
    [strikeNode runAction: blinkRandomly];
    
    
    return strikeNode;
}

- (SKShapeNode *)newShapeNodeRight {
    
    int radius = 25;
    
    //Generate random number for start time
    int lowerBound = 1000;
    int upperBound = 5000;
    int rndRightBallFlashCount = lowerBound + arc4random() % (upperBound - lowerBound);
    
    SKShapeNode *strikeNode = [[SKShapeNode alloc]init];
    strikeNode.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 2.0*radius, 2.0*radius)
                                                 cornerRadius:radius].CGPath;
    
    strikeNode.position = CGPointMake(350, 250);
    strikeNode.name = @"ballRight";
    
    if (rndRightBallFlashCount & 1) {
        strikeNode.fillColor = [UIColor greenColor];
    } else {
        strikeNode.fillColor = [UIColor redColor];
    }
    
    SKAction *blink = [SKAction sequence:@[
                                           [SKAction fadeOutWithDuration:0.05],
                                           [SKAction waitForDuration:0.5],
                                           [SKAction fadeInWithDuration:0.05],
                                           [SKAction waitForDuration:0.5]]];
    
    SKAction *fade = [SKAction sequence:@[[SKAction waitForDuration:5.0],
                                          [SKAction fadeOutWithDuration:0.05]]];
    
    SKAction *blinkRandomly = [SKAction repeatAction:blink count:(rndRightBallFlashCount/1000)];
    [strikeNode runAction:fade];
    [strikeNode runAction: blinkRandomly];
    
    
    return strikeNode;
}

- (SKShapeNode *)newShapeNodeStrike {
    
    int radius = 25;
    
    //Generate random number for start time
    int lowerBound = 1000;
    int upperBound = 5000;
    int rndStrikeFlashCount = lowerBound + arc4random() % (upperBound - lowerBound);

    
    SKShapeNode *strikeNode = [[SKShapeNode alloc]init];
    strikeNode.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 2.0*radius, 2.0*radius)
                                                 cornerRadius:radius].CGPath;
    
    strikeNode.position = CGPointMake(250, 250);
    
    if (rndStrikeFlashCount & 1) {
        strikeNode.fillColor = [UIColor greenColor];
    } else {
        strikeNode.fillColor = [UIColor redColor];
    }

    
    SKAction *blink = [SKAction sequence:@[
                                           [SKAction waitForDuration:0.5],
                                           [SKAction fadeOutWithDuration:0.05]]];
    
    
    [strikeNode runAction: blink];
    
    strikeNode.name = @"strike";
    
    
    return strikeNode;
    
}

- (SKShapeNode *)newShapeNodeTop {
    
    int radius = 25;
    
    //Generate random number for start time
    int lowerBound = 1000;
    int upperBound = 5000;
    int rndHighBallFlashCount = lowerBound + arc4random() % (upperBound - lowerBound);
    
    SKShapeNode *strikeNode = [[SKShapeNode alloc]init];
    strikeNode.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 2.0*radius, 2.0*radius)
                                                 cornerRadius:radius].CGPath;
    
    strikeNode.position = CGPointMake(250, 350);
    strikeNode.name = @"ballHigh";
    
    if (rndHighBallFlashCount & 1) {
        strikeNode.fillColor = [UIColor greenColor];
    } else {
        strikeNode.fillColor = [UIColor redColor];
    }
    
    SKAction *blink = [SKAction sequence:@[
                                           [SKAction fadeOutWithDuration:0.05],
                                           [SKAction waitForDuration:0.5],
                                           [SKAction fadeInWithDuration:0.05],
                                           [SKAction waitForDuration:0.5]]];
    
    SKAction *fade = [SKAction sequence:@[[SKAction waitForDuration:5.0],
                                          [SKAction fadeOutWithDuration:0.05]]];
    
    SKAction *blinkRandomly = [SKAction repeatAction:blink count:(rndHighBallFlashCount/1000)];
    [strikeNode runAction:fade];
    [strikeNode runAction: blinkRandomly];
    
    return strikeNode;
}

- (SKShapeNode *)newShapeNodeBottom {
    
    int radius = 25;
    
    //Generate random number for start time
    int lowerBound = 1000;
    int upperBound = 5000;
    int rndLowBallFlashCount = lowerBound + arc4random() % (upperBound - lowerBound);
    
    SKShapeNode *strikeNode = [[SKShapeNode alloc]init];
    strikeNode.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 2.0*radius, 2.0*radius)
                                                 cornerRadius:radius].CGPath;
    
    strikeNode.position = CGPointMake(250, 150);
    strikeNode.name = @"ballLow";
    
    if (rndLowBallFlashCount & 1) {
        strikeNode.fillColor = [UIColor greenColor];
    } else {
        strikeNode.fillColor = [UIColor redColor];
    }
    
    
    
    SKAction *blink = [SKAction sequence:@[
                                           [SKAction fadeOutWithDuration:0.05],
                                           [SKAction waitForDuration:0.5],
                                           [SKAction fadeInWithDuration:0.05],
                                           [SKAction waitForDuration:0.5]]];
    
    SKAction *fade = [SKAction sequence:@[[SKAction waitForDuration:5.0],
                                          [SKAction fadeOutWithDuration:0.05]]];
    
    SKAction *blinkRandomly = [SKAction repeatAction:blink count:(rndLowBallFlashCount/1000)];
    [strikeNode runAction:fade];
    [strikeNode runAction: blinkRandomly];
    
    return strikeNode;
}

#pragma mark User Interaction

-(void)didTouchPitchButton {
    
    swingButton.userInteractionEnabled = YES;
    
    //Remove any instantiated nodes
    SKAction *remove = [SKAction removeFromParent];
    
    SKNode *strike = [self childNodeWithName:@"strike"];
    SKNode *ballLeft = [self childNodeWithName:@"ballLeft"];
    SKNode *ballRight = [self childNodeWithName:@"ballRight"];
    SKNode *ballHigh = [self childNodeWithName:@"ballHigh"];
    SKNode *ballLow = [self childNodeWithName:@"ballLow"];
    SKNode *strikeLabel = [self childNodeWithName:@"swingMiss"];
    SKNode *hitLabel = [self childNodeWithName:@"baseHit"];
    SKNode *noSwingLabel = [self childNodeWithName:@"ball"];
    
    if (strike != nil) {
        [strike runAction:remove];
    }
    
    if (ballLeft != nil) {
        [ballLeft runAction:remove];
    }
    
    if (ballRight != nil) {
        [ballRight runAction:remove];
    }
    
    if (ballHigh != nil) {
        [ballHigh runAction:remove];
    }
    
    if (ballLow != nil) {
        [ballLow runAction:remove];
    }
    
    if (strikeLabel != nil) {
        [strikeLabel runAction:remove];
    }
    
    if (hitLabel != nil) {
        [hitLabel runAction:remove];
    }
    
    if (noSwingLabel != nil) {
        [noSwingLabel runAction:remove];
    }
    
    //Generate Random number for Node (target) presentation
    int lowerBound = 1000;
    int upperBound = 5000;
    
    rndValueStrike = lowerBound + arc4random() % (upperBound - lowerBound);
    rndValueBallLeft = lowerBound + arc4random() % (upperBound - lowerBound);
    rndValueBallRight = lowerBound + arc4random() % (upperBound - lowerBound);
    rndValueBallHigh = lowerBound + arc4random() % (upperBound - lowerBound);
    rndValueBallLow = lowerBound + arc4random() % (upperBound - lowerBound);
    
    NSLog(@"random number = %i, %i, %i, %i, %i", rndValueStrike, rndValueBallLeft, rndValueBallRight, rndValueBallHigh, rndValueBallLow);
    
    //present targets at varying (random) intervals
    [self performSelector:@selector(pitchStrike) withObject:nil afterDelay:rndValueStrike/1000];
    [self performSelector:@selector(pitchBallLeft) withObject:nil afterDelay:rndValueBallLeft/1000];
    [self performSelector:@selector(pitchBallRight) withObject:nil afterDelay:rndValueBallRight/1000];
    [self performSelector:@selector(pitchBallHigh) withObject:nil afterDelay:rndValueBallHigh/1000];
    [self performSelector:@selector(pitchBallLow) withObject:nil afterDelay:rndValueBallLow/1000];
    [self performSelector:@selector(userDidNotSwing) withObject:nil afterDelay:7.0];
    
}

-(void)pitchStrike {
    
    [self addChild: [self newShapeNodeStrike]];
    
    startTime = CACurrentMediaTime();
    
}

-(void)pitchBallLeft {
    
    [self addChild: [self newShapeNodeLeft]];
    
}

-(void)pitchBallRight {
    
    [self addChild: [self newShapeNodeRight]];
    
}

-(void)pitchBallHigh {
    
    [self addChild: [self newShapeNodeTop]];
    
}

-(void)pitchBallLow {
    
    [self addChild: [self newShapeNodeBottom]];
    
}


-(void)didTouchSwingButton {
    
    touchTime = CACurrentMediaTime() - startTime;
    
    SKNode *strike = [self childNodeWithName:@"strike"];
    
    if (strike == nil) {
        
        [self addChild:[self newSwingAndMissLabelNode]];
        
    } else if (strike != nil) {
        
        [self addChild:[self newBaseHitLabelNode]];
    }
    
    
    NSString *str = [NSString stringWithFormat:@"Reaction Time %.3f ms", (touchTime * 1000)];
    timeLabel.text = [NSString stringWithFormat: @"%@", str];
    
    swingButton.userInteractionEnabled = NO;
    
}

-(void)userDidNotSwing {
    
    SKNode *baseHit = [self childNodeWithName:@"baseHit"];
    SKNode *swingMiss = [self childNodeWithName:@"swingMiss"];
    
    if ((baseHit == nil) && (swingMiss == nil)) {
        if (rndValueStrike & 1) {
            [self addChild:[self newSwingAndMissLabelNode]];
        } else {
            [self addChild:[self newBallLabelNode]];
            
        }
        
    }
    
}



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    // Called when a touch begins
    /*
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
    }
     */
}



-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

@end
